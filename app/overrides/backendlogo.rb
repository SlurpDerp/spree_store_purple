Deface::Override.new(:virtual_path => "spree/layouts/admin",
    :name => "backend_logo",
    :replace      => 'erb[loud]:contains("spree/admin/shared/header")',
    :partial => "layouts/backend_navbar")
